# Connecting to the Duke Compute Cluster (DCC) 
The material for the presentation is found at
<https://gitlab.oit.duke.edu/kowzar/dcc-tutorial/>.

You will connect to the Duke Compute Cluster (DCC) login node,
its gateway, using your Duke NETID credentials. In the
instructions below, replace 'NETID' with your personal NETID.  To
connect, you need to use an ssh client from a terminal.  If
you are not using a Linux of BSD machine, see
<https://support.apple.com/guide/terminal/welcome/mac>
or
<https://docs.microsoft.com/en-us/powershell/scripting/overview?view=powershell-7.1>
for using a terminal. Navigating the DCC requires familiarity 
with the basics of the UNIX operating system. See <http://www.ee.surrey.ac.uk/Teaching/Unix/> for a tutorial.

## Step 1: Connect to the DCC login node from a terminal

```
$ ssh NETID@dcc-login.oit.duke.edu
```
If you are connecting from outside the Duke network, you will get a prompt to use MFA:

```
Duo two-factor login for NETID

Enter a passcode or select one of the following options:

 1. Duo Push to XXX-XXX-XXXX
 2. Phone call to XXX-XXX-XXXX
 3. SMS passcodes to XXX-XXX-XXX (next code starts with: 2)

Passcode or option (1-3):
```
## Step 2: Verify that you DCC biostat account is active
```
$ sacctmgr list associations User=NETID
```
If your account is active you will see the following output:
```
Cluster    Account       User 
 ---------- ---------- ----------
 dcc        biostat   NETID
```
If you are unable to connect to the DCC or if your account is not active,
please wait for instructions after Friday's presentation.

## Step 3: Set up ssh key
To facilitate the process of connecting your personal machine to the DCC,
consider using an ssh key. Once this is set up, you no longer need to enter
your password and complete MFA. 

At the terminal prompt type

```
$ ssh-keygen
```

This will prompt you to enter various options. If you are not familiar
with these options, respond to every prompt by hitting the Enter
button (that will use the default options).

This process will generate a text file named `id_rsa.pub`. On most
operating systems, this file is found under a subdirectory named .ssh (dot ssh)
under your home directory. This file contains a long string of
alphanumeric characters. 

Under most Linux systems, you will find this file under `/home/USERID/.ssh/` where
USERID is your UNIX user id. Under most Windows systems, you will find this file under `C:\Users\USERID\.ssh\` . [Need to add info about Apple OS].

Next, log into <https://idms-web.oit.duke.edu/portal/> and locate the
"Update your SSH public keys" text input window under "ADVANCED USER
OPTIONS". Copy and paste the string `id_rsa.pub` into the text
input window and be sure to save the changes.


Wait a few minutes and then attempt to ssh to the DCC login node. If
the login process skips the password prompt, then your key has been
successfully set up.
