# Notes
* This repository contains instructions for B&B students for using the Duke Compute Cluster (DCC) resources
* It also provides a number of sample scripts for batch processing
* Do NOT under any circumstance store PHI or sensitive data on the DCC. 
* See the Appropriate Use and  Data Security and Privacy sections in <https://dcc.duke.edu/dcc/> for more detailed notes on acceptable use of the DCC
* Be sure to review Tom Milledge's excellent tutorial available from the top of the page of <https://dcc.duke.edu/dcc/slurm/>. The material shown here is mostly taken from these slides.
* This short tutorial was designed to help get you started. See The slides above and <https://dcc.duke.edu/dcc/> for more detailed information.
* To navigate the DCC, you will have to be familiar with the basics of the UNIX operating system. See <http://www.ee.surrey.ac.uk/Teaching/Unix/> for a tutorial.

# SLURM terminology
* The DCC can be thought of as a collection of computer servers. We refer to each server as a node
* The DCC is split into paritions
* The DCC uses SLURM (Simple Linux Utility for Resource Management) as its job scheduling system
* A partition is a group of nodes
* Each user has at least one account. The account will among other things determine the priveleges a user has within a given partition
* As B&B students, you will have a 'biostat' account. When using this account, you will have privileges on the nodes of the 'biostat' partition
* As B&B students, you will also have privileges on the codes in the 'common' partition. You will also be able to submit jobs to the 'scavenger' partition
* To get detailed information on the SLURM architecture, features and commands see <https://slurm.schedmd.com/> and <https://dcc.duke.edu/dcc/slurm/>

# Partitions
* biostat: the B&B partition currently consists of five nodes (dcc-biostat-[01-05])
* dcc-biostat-[04-05] are recently installed nodes (Spring 2022)
* dcc-biostat-gpu-[01-02] are recently installed GPU nodes (Fall 2022)
* common: DCC owned CPU nodes (up to 64GB of RAM)
* gpu-common: DCC owned GPU nodes
* scavenger: for jobs to be run on lab-owned CPU nodes with low priority
* scavenger-gpu: for jobs to be run on lab-owned GPU nodes with low priority

# Log into the DCC 
You will interact with the DCC through a
login node:
* dcc-login.oit.duke.edu

You will to connect to one of these two nodes using an ssh client. The easiest
approach is to use an ssh application from a terminal (available in Linux, MacOS
and Windows) 

$ ssh NETID@dcc-login.oit.duke.edu  


Notes: 
* Do not use the login nodes for any computational task (including compiling
your programs).
* The login nodes are only to be used to submit and manage your compute jobs
(or for editing or light file system operations) 
* If you are outside the Duke firewall, you need to use MFA to authenticate
(after submitting your password, you will be prompted to enter an SMS passcode
or to MFA through phone or Duo).
* Alternatively, you can ssh through VPN to skip
the MFA.

# Work on DCC interactively
You can submit in batch mode or interactively (remember to first
login into one of the two login nodes). To work on the DCC
interactively, you have to request a shell (through the login node) 
```
$ srun --pty bash -i
```
You should now have a bash prompt showing the node that was
allocated for you by SLURM



# Software Modules

Software packages on the DCC are managed as modules. You need to
load a relevant module before you can use the software.

## Get a list available modules
```
$ module avail
```
## Open an interactive shell (through the login node) 
```
$ srun --pty bash -i
```

## Add an available module (R version 3.6.3 in this case)
```
$ module load R/3.6.3
```

## Verify that program is loaded
```
$ R --version
```

## Unload the module
```
$ module unload R/3.6.3
```
# Two interactive examples

## Compile and run C++ example
```
$ srun --pty bash -i

$ module load GCC/7.4.0

$ g++ --version

$ g++ -o mytestcpp mytestcpp.cpp 

$ ./mytestcpp

$ module unload GCC/7.4.0
```

## R interactive analysis
Note that '$' is denotes the UNIX prompt while '>' denotes the R prompt
```
$ srun --pty bash -i  

$ module load R/3.6.3 

$ R --version

$ R

> set.seed(12345)

> mydat <- rnorm(100)

> summary(mydat)

> q(save = "no")

$ module unload R/3.6.3
```

# Submit batch jobs on DCC
To use batch mode, you have to write a script and submit it using
the sbatch command
## C++ example
The mytestcpp.q script in the repository will compile and run the CPP toy example mytestcpp.cpp
```
$ sbatch mytestcpp.q
```
## R example
```
$ sbatch mytestR.q 
```


# Examples of customized submissions
In the previous section, we had SLURM pick the node for your
job. This can be customized by specifying specific paritions
and nodes.
Note that by default your account is 'biostat'. You specify the account,
partition and node using the -A (--account), -p (--partition)
and -w (--nodelist) flags respectively
## Submit your batch job to the dcc-biostat-01 node on the biostat partition
```
$ sbatch -A biostat -p biostat -w dcc-biostat-01 mytestR.q
```
## Submit a batch job to the common partition
```
$ sbatch -p common mytestR.q
```
## To run interactive shell on CPU node allocate 4GB of RAM
```
$ srun --mem=4G --pty bash -i
```
## To run interactive shell on CPU node on a specific  B&B node
```
$ srun -p biostat -w dcc-biostat-02 -A biostat --pty bash -i
```
## Submit a batch job to the common partition
```
$ sbatch -p common mytestR.q
```




# Manage your jobs

## Check the status of your jobs
```
$ squeue -u NETID
```
## Get information about a job
```
$ scontrol show job JOBID
```
## Cancel a job
```
$ scancel JOBID
```
## Get accounting data
```
$ sacct
```

# Administrative
## List users in a group
```
$ sacctmgr list associations Account=biostat
```
## Check access for user
```
$ sacctmgr list associations User=NETID
```
## Check state of nodes in the biostat partition
```
$ sinfo --long --partition=biostat
```
## Get information (e.g., core count and memory) for a specific node
```
$ scontrol show node dcc-biostat-02
```
## Check state of nodes in the common partition
```
$ sinfo --long --partition=common
```
## Compare hardware configurations of two nodes (01 and 317) from the common Partitions
```
$ scontrol show node dcc-core-01
$ scontrol show node dcc-core-317
```
# Utilities
Some helpful utlility programs available 
* SCM: hg (/opt/apps/Python-2.7.10/bin/hg), git
* editors:   emacs, nano, vi
* File transfer: wget, rsync, sftp, scp
* compilers: gcc, gfortran, javac
* programming languages : python, ruby, scala

# Uploading and downloading files and directories
## Upload file to machine
```
$ scp foo.txt NETID@dcc-login.oit.duke.edu: 
```
## Upload directory
```
$ scp -r mylocaldir/ NETID@dcc-login.oit.duke.edu: 
```
## Download file to local machine
```
$ scp NETID@dcc-login.oit.duke.edu:foo.txt . 
```
## Download directory to local machine
```
$ scp -r NETID@dcc-login.oit.duke.edu:remotedir/ . 
```

# Using CUDA
## Check for GPU card (after running interactive shell on GPU node)
```
$ nvidia-smi -q
```
## Set CUDA paths (check to make sure that the path exists)
```
$ export LD_LIBRARY_PATH=/usr/local/cuda/lib64
#
$ export PATH=/usr/local/cuda/bin:$PATH
```
Run CUDA jobs on the common GPU nodes
## Batch mode
```
$ sbatch  -p gpu-common --gres=gpu myscript-CUDA.q
```
## Interactive mode
```
$ srun -p gpu-common  --gres=gpu --pty bash -i
```

# Storage

See <https://dcc.duke.edu/dcc/files/> for details.

## Home directory
Your home directory on the DCC will be /hpc/group/biostat/NETID/ . It is not recommended to use this
partition for your research. Use it to keep important files. To check your storage
```
$ du -hd1 ~/
```
## Scratch space

You can create a folder under /work to temporarily save large files. This is a high-speed partition. These files will not be backed up and will automatically deleted depending on age of the files and available capacity. To check your storage 
```
$ storage-report -u NETID
```

## Personal HPC storage

Use this space (/hpc/home/NETID) for your scripts an environments. You quota is 10GB. To check your usage

```
du -hdl /hpc/home/NETID/
```

## Group HPC storage
As biostat users, you will also have access to our departmental HPC storage (1TB)
To check available to storage:

```
$ df -h /hpc/group/biostat
```

## Archiving

You can create a folder under /datacommons/biostat for archiving important files. These files are currently NOT backed up. Your ability to archive files will be limited
by a departmental quota. To check your storage
```
$ df -h /datacommons/biostat/NETID/
```

# Using Duke OpenOnDemand
* Duke's OpenOnDemand instance allows users to access select computing environments from a web browser
* Access this resource using your web browser at <https://dcc-ondemand-01.oit.duke.edu/>
* See <https://oit-rc.pages.oit.duke.edu/rcsupportdocs/OpenOnDemand/GettingStarted/> for additional details
