import tensorflow as tf

# Check tensorflow version
print(tf.version.VERSION)

# Check if GPU is available (when using a GPU node)
tf.config.list_physical_devices('GPU')
