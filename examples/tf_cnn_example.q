#!/bin/bash
#
#SBATCH --job-name=tfcnnexample
#SBATCH --mail-user=
#SBATCH --mail-type=END,FAIL
#SBATCH --mem=8G
#SBATCH --time=03:00:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --output=tfcnnexample-%J.stdout
#SBATCH --error=tfcnnexample-%J.stderr

module load Python-GPU/3.7.6

pip install tensorflow_datasets

# Check nvidia driver
nvidia-smi -q

python tf_cnn_example.py

module unload Python-GPU/3.7.6
