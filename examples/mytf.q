#!/bin/bash
#
#SBATCH --job-name=mytfjob
#SBATCH --mail-user=
#SBATCH --mail-type=END,FAIL
#SBATCH --mem=4G
#SBATCH --time=03:00:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --output=slurm-R-job-%J.stdout
#SBATCH --error=slurm-R-job-%J.stderr

module load Python-GPU/3.7.6

python mytf.py

module unload Python-GPU/3.7.6
