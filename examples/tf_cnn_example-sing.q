#!/bin/bash
#
#SBATCH --job-name=tfcnnexample
#SBATCH --mail-user=
#SBATCH --mail-type=END,FAIL
#SBATCH --mem=8G
#SBATCH --time=03:00:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --output=tfcnnexample-%J.stdout
#SBATCH --error=tfcnnexample-%J.stderr


mysif=/work/owzar001/singularity/bios-823-container-gpu.sif

# Check nvidia driver
nvidia-smi -q

singularity \
  exec \
  --nv \
  ${mysif} \
  /opt/pydatasci/bin/python3  tf_cnn_example-sing.py
