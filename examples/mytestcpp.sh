#!/bin/bash

# Get version of c++ compiler
g++ --version

# Compile code
g++ -o mytestcpp.out mytestcpp.cpp

# Execute
./mytestcpp.out
